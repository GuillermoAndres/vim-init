# Instalacion Linux 
Para sistemas operativos linux la instalacion se hara con sus respectivo gestor
de paquetes:
~~~
sudo apt-get install vim
~~~
Una vez instalado podremos utilizarlo. Por defecto en algunas distribuciones de
Linux (Debian/Fedora) el clipboard no funcionara, para activarlo se necesita
instalar el paquete:
~~~
# Debian
sudo apt-get install vim-gtk
~~~

https://vi.stackexchange.com/questions/84/how-can-i-copy-text-to-the-system-clipboard-from-vim
https://askubuntu.com/questions/347519/unable-to-copy-from-vim-to-system-clipboard

Una vez instalado podremos ahora utilizarlo perfectamente, si desea verificar que
esta activado es con:

~~~
vim --version | grep clipboard
~~~
https://stackoverflow.com/questions/40735215/copy-paste-from-to-clipboard-in-vim-stopped-working

Esto nos aparecera `+clipboard` que significa que esta habilitado.


## Plugins
1. Para instalar los plugins debemos de instalar `Plug` que es un gestor de plugins:
https://github.com/junegunn/vim-plug

1. Debemos de instalar nodejs para instalar Coc (Debe ser root sudo -i):
~~~
curl -sL install-node.vercel.app/lts | bash
~~~
https://github.com/neoclide/coc.nvim

1. Una vez instalado las dos cosas solo sera cuestion de escribir `:PlugInstall`



