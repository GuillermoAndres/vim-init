set nocompatible        " be iMproved, required
filetype off            " required
syntax on			    " Enable colors 
" Enable cursor block and bar steady in Normal and Insert mode
let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"

" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
"Instalarlo antes de usar, @see:  https://github.com/junegunn/vim-plug

Plug 'neoclide/coc.nvim', {'branch': 'release'} "Coc servers
Plug 'zubairakram/emmet-vim/', {'branch': 'html5-snippet'}
Plug 'jiangmiao/auto-pairs'
" Plug 'terryma/vim-expand-region'
" Plug 'tpope/vim-commentary'
" Plug 'SirVer/ultisnips'
" Plug 'mg979/vim-visual-multi', {'branch': 'master'}
" Plug 'ctrlpvim/ctrlp.vim'
" Plug 'junegunn/goyo.vim'
" Plug 'tommcdo/vim-exchange'
" https://stackoverflow.com/questions/48721114/is-there-a-vim-command-that-swap-two-words-with-crossing-one-operator
" Temas (colorscheme)
"Plug 'pgavlin/pulumi.vim'
"Plug 'embark-theme/vim', { 'as': 'embark' }

call plug#end()
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
" filetype plugin on

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

"--------------------------Configuration Coc plugin--------------------------------------

"Configuracion para coc, para los pop-ups 
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
     \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()

"Para puedas visualizar las opciones en cualquier lugar de la palabra
"Control-space
" use <c-space>for trigger completion
inoremap <silent><expr> <c-space> coc#refresh()
" use <c-space>for trigger completion
inoremap <silent><expr> <NUL> coc#refresh()

"Navegar opciones  hacia adelante y hacia atras con Tab y Shift Tab.
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"Use <cr> to confirm completion
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

"Que la primera ocurrecia de pop-up() sea selacionada automaticamente al
"presionar enter
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

"Close the preview window when completion is done.
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif


" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)


" Extensiones instaladas
" CocInstall coc-html coc-css coc-tsserver coc-phpls coc-snippets coc-clangd 
" (primero debes de instalar clangd, debe estar en tu path)
" Se guardan en la ruta: 
" ~/.config/coc/extensions/node_modules/coc-html

" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions  (checar por
" si quieres deshabilitar una extension)
" https://langserver.org/    (Pagina donde hay los language server for any
" editor)
"coc-clangd:     C y C++ language
"coc-html  :     Te recomienda etiquetas y descripcion una vez que hacer tag apertura <label...
"coc-css   :     autocompletado y ya no se necesita preview-color,viene integrado
"coc-java  :     Instalar jdt version 57 para el error que podria causar el
"servidor, ver issues de la su pagina de github, solo es descargar jdt
"descomprimir, buscar la carpeta del jdt y borrar archivos y poner los
"descargados 

" => General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set guioptions-=m  		" remove menu bar
set guioptions-=T  		" remove toolbar
set guioptions-=r  		" remove right-hand scroll bar
set guioptions-=L  		" remove left-hand scroll bar
set path+=**			" Searches current directory recursively.
set wildmenu			" Display all matches when tab complete.
set incsearch                   " Incremental search
set hidden                      " Needed to keep multiple buffers open
set nobackup                    " No auto backups
set noswapfile                  " No swap
set t_Co=256                    " Set if term supports 256 colors.
set number                      " Display line numbers
set clipboard=unnamedplus       " Copy/paste between vim and other programs.


"You can toggle relative numbering on and off using:
"set relativenumber      	" Show relative line numbers
"set number			" Display line numbers
":set rnu    			" toggle relative numbering on
":set rnu!   			" toggle relative numbering off

set encoding=utf-8 		" Codifica los caractares para ser mostrados como los acentos o ñ
set tabstop=4 			"Para los tabuladores size
set expandtab 			"Replacing Tabs with White Spaces
set softtabstop=4 		"Removing Multiple Spaces on Single Backspace

"So, 4 white spaces for each tab will be removed all together with a single press of the <Backspace> key.

"Estos tres van acompados para que funcione perfecto, shiftwidtj ajusta el

"identato de autoindent, en este caso cuatro

set autoindent 			"Enabling Automatic Indentation,indenta dependiendo del anterior

set smartindent 		" even better autoindent (e.g. add indent after '{')

set shiftwidth=4 		" number of spaces to use for each step of (auto)indent

set shiftround

set hidden  			" Permitir cambiar de buffers sin tener que guardarlos

set mouse=a " enable mouse support (might not work well on Mac OS X), para cambiarme de ventanas
" https://unix.stackexchange.com/questions/139578/copy-paste-for-vim-is-not-working-when-mouse-set-mouse-a-is-on

"set title  " Muestra el nombre del archivo en la ventana de la terminal

set nowrap  			" No dividir la línea si es muy larga

"set cursorline  		" Resalta la línea actual

"set colorcolumn=80  	" Muestra la columna límite a 80 caracteres

set ignorecase 			" Ignorar mayúsculas al hacer una búsqueda

set smartcase  			" No ignorar mayúsculas si la palabra a buscar contiene mayúsculas

"set spelllang=en,es		" Corregir palabras usando diccionarios en inglés y español

"set background=dark		" Fondo del tema: light o dark

"Genera una paleta de colores diferente, haciendo como un resaltado
"set termguicolors "Activa true colors en la terminal utilizando guifg in vez de termfg.

" Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode

" Ayuda mucho cambiar tu directorio actual, sera donde se encuentre el archivo
" que se abrira, me ha ayuda para Nerdtree, cuando se untiliza vim
" ruta/archivo.cpp ese ahora sera tu directorio actual. 
set autochdir

" Vim auto line-break
set textwidth=79

"-----------------------------Mappings-------------------------------------------------- 
" <S-Z-Z> - Guarda y sale
" <S-Z-Q> - Sale sin guardar
" <C-c>   - normal mode
" S-shift
" La 'a' significa all buffers 

"Entrar NORMAL mode
imap <C-s> <ESC>
"Guardar
nnoremap <C-s> :w<CR> 
" Quit discarding changes
" inoremap <C-q> <esc>:qa!<cr>               
" nnoremap zz :q<cr> (Se sobre escribe con :h zz)
" nnoremap zq :qa!<cr>

"Definir la letra leader
let g:mapleader = ' '  " Definir espacio como la tecla líder
" https://stsewd.dev/es/posts/neovim-installation-configuration/
nnoremap <leader>w :w<CR>

"----- Management buffers ------------
nnoremap <leader><Left> :bprevious<CR>
nnoremap <leader><Right> :bnext<CR>
" nnoremap <leader>d :bd<CR>
" %a for current
" h for hidden
" # for previous
" + for modified.

" Moverse al buffer siguiente con <líder> + l
nnoremap <leader>l :bnext<CR>
" Moverse al buffer anterior con <líder> + j
nnoremap <leader>h :bprevious<CR>
" Cerrar el buffer actual con <líder> + q
nnoremap <leader>q :bdelete<CR>

"noremap <Leader>y "*y -

set splitbelow splitright
" <C-w> v : Split vertical
" <C-w> s : Split horizontal
" <C-w> j : Movemnt left
" <C-w> k : Movemnt down
" <C-w> l : Movemnt right

" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>


